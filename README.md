# SA-IS

“Linear Suffix Array Construction by Almost Pure Induced-Sorting” by G. Nong, S. Zhang and W. H. Chan

```
Benchmarks:
Intel(R) Core(TM) i3-4160 CPU @
3.60 Ghz 
16 GiB
MinGW Release

alice29.txt
Standard text from book 'Alice in Wonderland'
read 100000 bytes
naive 100 times:
naive time 0.0233393 s per loop = 233.393 ns per symbol
algorithm 100 times:
algorithm time 0.0062398 s per loop = 62.398 ns per symbol
3.7404 x faster
both methods created the same SA

mtpacga
Sample gene, almost random 4/256 values
read 100000 bytes
naive 100 times:
naive time 0.0256191 s per loop = 256.191 ns per symbol
algorithm 100 times:
algorithm time 0.00646988 s per loop = 64.6988 ns per symbol
3.95976 x faster
both methods created the same SA

random.txt
Generated pseudo random text, 64/256 values
read 100000 bytes
naive 100 times:
naive time 0.0182494 s per loop = 182.494 ns per symbol
algorithm 100 times:
algorithm time 0.0061898 s per loop = 61.898 ns per symbol
2.9483 x faster
both methods created the same SA

abac
Long cycle, problem for naive
read 30000 bytes
naive 1 times:
naive time 0.273 s per loop = 9100 ns per symbol
algorithm 1000 times:
algorithm time 0.000812973 s per loop = 27.0991 ns per symbol
335.804 x faster
both methods created the same SA
```

can be seen inversely proportional efficency when cicles exists.
Naive takes long time when long cycles, but algorithm SA-IS especially well.

**License**: Apache 2.0 concerns my files; papers, python or samples have own licenses.