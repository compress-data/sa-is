//
// Created by Andrzej Borucki on 2019-07-05
//
#include <vector>
#include <algorithm>
#include <cstdint>
#include <cassert>
#include <tuple>
#include <chrono>
#include <iostream>
#ifdef _MSC_VER
#include <Windows.h>
#else
#include <dirent.h>
#endif

using namespace std;

class stopwatch
{
public:
	chrono::time_point<chrono::high_resolution_clock> a, b;
	void start() { a = chrono::high_resolution_clock::now(); }
	void stop() { b = chrono::high_resolution_clock::now(); }
	double duration()
	{
		chrono::duration<double> elapsed_seconds = b - a;
		return elapsed_seconds.count();
	}
};

typedef uint32_t DictType;
vector<DictType> sa, sa_base;
enum MapType { S_TYPE, L_TYPE };
const DictType Eos = DictType(-1);

//difference from memcmp: is strings are different lengths, and not difference found:
//smaller is shorter string
int bcomp(unsigned char* buf0, unsigned char* buf1, int size0, int size1)
{
    if (size0==0 || size1==0)
    {
        if (size0==0 && size1==0)
            return 0;
        else if (size0==0)
            return -1;
        else return 1;
    }
    else if (buf0[0] != buf1[0])
    {
        if (buf0[0] < buf1[0])
            return -1;
        else
            return 1;
    }
    int memres = memcmp(buf0+1, buf1+1, min(size0, size1)-1);
    if (memres==0)
    {
        if (size0 < size1)
            return -1;
        else if (size0 > size1)
            return 1;
        else return 0;
    }
    else
        return memres;
}

//Builds a map marking each suffix of the data as S_TYPE or L_TYPE.
vector<MapType> buildtypeMap(vector<DictType>& string)
{
	/* The map should contain one more entry than there are characters
	   in the string, because we also need to store the type of the
	   empty suffix between the last characterand the end of the
	   string. */
	vector<MapType> res(string.size() + 1);

	// The empty suffix after the last character is S_TYPE
	res.back() = S_TYPE;

	// If this is an empty string
	// there are no more characters, so we're done.
	if (string.size() == 0) return res;

	// The suffix containing only the last character must necessarily
	// be larger than the empty suffix.
	res[res.size() - 2] = L_TYPE;

	// Step through the rest of the string from right to left.
	for (int i = string.size() - 2; i >= 0; i--)
	{
		if (string[i] > string[i + 1])
			res[i] = L_TYPE;
		else if (string[i] == string[i + 1] && res[i + 1] == L_TYPE)
			res[i] = L_TYPE;
		else
			res[i] = S_TYPE;
	}
	return res;
}

//Returns true if the character at offset is a left-most S-type.
bool isLMSChar(DictType offset, vector<MapType>& typeMap)
{
	if (offset == 0)
		return false;
	if (typeMap[offset] == S_TYPE && typeMap[offset - 1] == L_TYPE)
		return true;

	return false;
}

//Return true if LMS substrings at offsetAand offsetB are equal.
bool lmsSubstringsAreEqual(vector<DictType>& string, vector<MapType>& typeMap, DictType offsetA, DictType offsetB)
{
	// No other substring is equal to the empty suffix.
	if (offsetA == string.size() or offsetB == string.size())
		return false;

	int i = 0;
	while (true)
	{
		bool aIsLMS = isLMSChar(i + offsetA, typeMap);
		bool bIsLMS = isLMSChar(i + offsetB, typeMap);

		// If we've found the start of the next LMS substrings
		if (i > 0 and aIsLMS and bIsLMS)
		{
			// then we made it all the way through our original LMS
			// substrings without finding a difference, so we can go
			// home now.
			return true;
		}

		if (aIsLMS != bIsLMS)
			// We found the end of one LMS substring before we reached
			// the end of the other.
			return false;

		if (string[i + offsetA] != string[i + offsetB])
			// We found a character difference, we're done.
			return false;
		i += 1;
	}
}

vector<DictType> findBucketSizes(vector<DictType>& string, int alphabetSize = 256)
{
	vector<DictType> res(alphabetSize);
	for (int i = 0; i < string.size(); i++)
		res[string[i]]++;
	return res;
}

vector<DictType> findBucketHeads(vector<DictType>& bucketSizes)
{
	vector<DictType> res(bucketSizes.size());
	DictType offset = 1;
	for (int i = 0; i < res.size(); i++)
	{
		DictType size = bucketSizes[i];
		res[i] = offset;
		offset += size;
	}
	return res;
}

vector<DictType> findBucketTails(vector<DictType>& bucketSizes)
{
	vector<DictType> res(bucketSizes.size());
	DictType offset = 1;
	for (int i = 0; i < res.size(); i++)
	{
		DictType size = bucketSizes[i];
		offset += size;
		res[i] = offset - 1;
	}
	return res;
}

//Make a suffix array with LMS-substrings approximately right.
vector<DictType> guessLMSSort(vector<DictType>& string, vector<DictType>& bucketSizes, vector<MapType>& typeMap)
{
	// Create a suffix array with room for a pointer to every suffix of
	// the string, including the empty suffix at the end.
	vector<DictType> guessedSuffixArray(string.size() + 1, Eos);
	vector<DictType> bucketTails = findBucketTails(bucketSizes);

	// Bucket - sort all the LMS suffixes into their appropriate bucket.
	for (int i = 0; i < string.size(); i++)
	{
		if (!isLMSChar(i, typeMap))
			// Not the start of an LMS suffix
			continue;

		// Which bucket does this suffix go into ?
		int bucketIndex = string[i];
		// Add the start position at the tail of the bucket
		guessedSuffixArray[bucketTails[bucketIndex]] = i;
		// and move the tail pointer down.
		assert(bucketTails[bucketIndex] > 0);
		bucketTails[bucketIndex]--;

		// Show the current state of the array
		// showSuffixArray(guessedSuffixArray)
	}
	// The empty suffix is defined to be an LMS - substring, and we know
	// it goes at the front.
	guessedSuffixArray[0] = string.size();
	//showSuffixArray(guessedSuffixArray)

	return guessedSuffixArray;
}

//Slot L-type suffixes into place.
void induceSortL(vector<DictType>& string, vector<DictType>& guessedSuffixArray, vector<DictType>& bucketSizes, vector<MapType>& typeMap)
{
	vector<DictType> bucketHeads = findBucketHeads(bucketSizes);
	// For each cell in the suffix array.
	for (int i = 0; i < guessedSuffixArray.size(); i++)
	{
		if (guessedSuffixArray[i] == -1)
			// No offset is recorded here.
			continue;

		// We're interested in the suffix that begins to the left of
			// the suffix this entry points at.
		int j = guessedSuffixArray[i] - 1;
		if (j < 0)
			// This entry in the suffix array is the suffix that begins
			// at the start of the string, offset 0. Therefore there is
			// no suffix to the left of it, and j is out of bounds of
			// the typeMap.
			continue;

		if (typeMap[j] != L_TYPE)
			// We're only interested in L-type suffixes right now.
			continue;
		// Which bucket does this suffix go into ?
		int bucketIndex = string[j];
		// Add the start position at the head of the bucket
		guessedSuffixArray[bucketHeads[bucketIndex]] = j;
		// and move the head pointer up.
		bucketHeads[bucketIndex] += 1;
	}
}

//Slot S-type suffixes into place.
void induceSortS(vector<DictType>& string, vector<DictType>& guessedSuffixArray, vector<DictType>& bucketSizes, vector<MapType>& typeMap)
{
	vector<DictType> bucketTails = findBucketTails(bucketSizes);
	for (int i = guessedSuffixArray.size() - 1; i >= 0; i--)
	{
		int j = guessedSuffixArray[i] - 1;
		if (j < 0)
			// This entry in the suffix array is the suffix that begins
			// at the start of the string, offset 0. Therefore there is
			// no suffix to the left of it, and j is out of bounds of
			// the typeMap.
			continue;
		if (typeMap[j] != S_TYPE)
			// We're only interested in S-type suffixes right now.
			continue;

		// Which bucket does this suffix go into ?
		int bucketIndex = string[j];
		// Add the start position at the tail of the bucket
		guessedSuffixArray[bucketTails[bucketIndex]] = j;
		// and move the tail pointer down.
		bucketTails[bucketIndex] -= 1;
	}
}

//Construct a 'summary string' of the positions of LMS-substrings.
tuple<vector<DictType>, int, vector<DictType>>
summariseSuffixArray(vector<DictType>& string, vector<DictType>& guessedSuffixArray, vector<MapType>& typeMap)
{
	const int None = 0;
	// We will use this array to store the names of LMS substrings in
	// the positions they appear in the original string.
	vector<DictType> lmsNames(string.size() + 1, Eos);


	// Keep track of what names we've allocated.
	int currentName = 0;

	// Where in the original string was the last LMS suffix we checked ?
	int lastLMSSuffixOffset = None;

	// We know that the first LMS - substring we'll see will always be
	// the one representing the empty suffix, and it will always be at
	// position 0 of suffixOffset.
	lmsNames[guessedSuffixArray[0]] = currentName;
	lastLMSSuffixOffset = guessedSuffixArray[0];

	// For each suffix in the suffix array
	for (int i = 1; i < guessedSuffixArray.size(); i++)
	{
		// where does this suffix appear in the original string ?
		int suffixOffset = guessedSuffixArray[i];

		// We only care about LMS suffixes.
		if (!isLMSChar(suffixOffset, typeMap))
			continue;

		// If this LMS suffix starts with a different LMS substring
		// from the last suffix we looked at.
		if (!lmsSubstringsAreEqual(string, typeMap, lastLMSSuffixOffset, suffixOffset))
			// then it gets a new name
			currentName++;

		// Record the last LMS suffix we looked at.
		lastLMSSuffixOffset = suffixOffset;

		// Store the name of this LMS suffix in lmsNames, in the same
		// place this suffix occurs in the original string.
		lmsNames[suffixOffset] = currentName;
	}
	// Now lmsNames contains all the characters of the suffix string in
	// the correct order, but it also contains a lot of unused indexes
	// we don't care about and which we want to remove. We also take
	// this opportunity to build summarySuffixOffsets, which tells
	// us which LMS - suffix each item in the summary string represents.
	// This will be important later.
	vector<DictType> summarySuffixOffsets;
	vector<DictType> summaryString;
	for (DictType index = 0; index < lmsNames.size(); index++)
		//for index, name in enumerate() :
	{
		DictType name = lmsNames[index];
		if (name == -1)
			continue;
		summarySuffixOffsets.push_back(index);
		summaryString.push_back(name);
	}

	// The alphabetically smallest character in the summary string
	// is numbered zero, so the total number of characters in our
	// alphabet is one larger than the largest numbered character.
	int summaryAlphabetSize = currentName + 1;

	//return result;// 
	return make_tuple(summaryString, summaryAlphabetSize, summarySuffixOffsets);
}

//Construct a sorted suffix array of the summary string.

// Make a suffix array with LMS suffixes exactly right.
vector<DictType> accurateLMSSort(vector<DictType>& string, vector<DictType>& bucketSizes, vector<MapType>& typeMap,
	vector<DictType>& summarySuffixArray, vector<DictType>& summarySuffixOffsets)
{
	// A suffix for every character, plus the empty suffix.
	vector<DictType> suffixOffsets(string.size() + 1, Eos);

	// As before, we'll be adding suffixes to the ends of their
	// respective buckets, so to keep them in the right order we'll
	// have to iterate through summarySuffixArray in reverse order.
	vector<DictType> bucketTails = findBucketTails(bucketSizes);
	for (int i = summarySuffixArray.size() - 1; i > 1; i--) //fixme i>1 czy i>=1?
	{
		int stringIndex = summarySuffixOffsets[summarySuffixArray[i]];

		// Which bucket does this suffix go into ?
		int bucketIndex = string[stringIndex];
		//Add the suffix at the tail of the bucket
		suffixOffsets[bucketTails[bucketIndex]] = stringIndex;
		//and move the tail pointer down.
		bucketTails[bucketIndex] -= 1;
	}
	// Always include the empty suffix at the beginning.
	suffixOffsets[0] = string.size();
	return suffixOffsets;
}

vector<DictType> makeSuffixArrayByInducedSorting(vector<DictType>& string, int alphabetSize);

vector<DictType> makeSummarySuffixArray(vector<DictType>& summaryString, int summaryAlphabetSize)
{
	vector<DictType> summarySuffixArray;
	if (summaryAlphabetSize == summaryString.size())
	{
		// Every character of this summary string appears once and only
		// once, so we can make the suffix array with a bucket sort.		
		summarySuffixArray.resize(summaryString.size() + 1, Eos);

		// Always include the empty suffix at the beginning.
		summarySuffixArray[0] = summaryString.size();
		for (int x = 0; x < summaryString.size(); x++)
		{
			DictType y = summaryString[x];
			summarySuffixArray[y + 1] = x;
		}
	}
	else
	{
		// This summary string is a little more complex, so we'll have
		// to use recursion.
		summarySuffixArray = makeSuffixArrayByInducedSorting(summaryString, summaryAlphabetSize);
	}
	return summarySuffixArray;
}

//Compute the suffix array of 'string' with the SA-IS algorithm.
vector<DictType> makeSuffixArrayByInducedSorting(vector<DictType>& string, int alphabetSize)
{
	// Classify each character of the string as S_TYPE or L_TYPE
	vector<MapType> typeMap = buildtypeMap(string);

	// We'll be slotting suffixes into buckets according to what
	// character they start with, so let's precompute that info now.
	vector<DictType> bucketSizes = findBucketSizes(string, alphabetSize);

	// Use a simple bucket - sort to insert all the LMS suffixes into
	// approximately the right place the suffix array.
	vector<DictType>guessedSuffixArray = guessLMSSort(string, bucketSizes, typeMap);

	// Slot all the other suffixes into guessedSuffixArray, by using
	// induced sorting.This may move the LMS suffixes around.
	induceSortL(string, guessedSuffixArray, bucketSizes, typeMap);
	induceSortS(string, guessedSuffixArray, bucketSizes, typeMap);

	// Create a new string that summarises the relative order of LMS
	// suffixes in the guessed suffix array.
	vector<DictType> summaryString;
	int summaryAlphabetSize;
	vector<DictType> summarySuffixOffsets;
	tie(summaryString, summaryAlphabetSize, summarySuffixOffsets) =
		summariseSuffixArray(string, guessedSuffixArray, typeMap);

	// Make a sorted suffix array of the summary string.
	vector<DictType> summarySuffixArray = makeSummarySuffixArray(summaryString, summaryAlphabetSize);

	// Using the suffix array of the summary string, determine exactly
	// where the LMS suffixes should go in our final array.
	vector<DictType> result = accurateLMSSort(string, bucketSizes, typeMap,
		summarySuffixArray, summarySuffixOffsets);

	// and once again, slot all the other suffixes into place with
	// induced sorting.
	induceSortL(string, result, bucketSizes, typeMap);
	induceSortS(string, result, bucketSizes, typeMap);

	return result;
}

std::string current_working_directory()
{
#ifdef _MSC_VER
	TCHAR NPath[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, NPath);
	wstring wstr = NPath;
	string working_directory(wstr.begin(), wstr.end());
#else
	char* cwd = _getcwd(0, 0);
	std::string working_directory(cwd);
	std::free(cwd);
#endif
	return working_directory;
}

string relativeFolder =
#ifdef _MSC_VER
        "../res/";
#else
        "../../res/";
#endif

struct Source {
	unsigned char* buf;
	int buffer_len;
	int win_start = 0;
	int win_end;

	Source(string filename, int buffer_len) {
		buf = new unsigned char[buffer_len];

		string path = relativeFolder + filename;
		FILE* fp;
		fopen_s(&fp, path.c_str(), "rb");
		if (!fp)
		{
			cout << "can't oppen " << path << endl;
			exit(1);
		}
		fread(buf, 1, buffer_len, fp);
		fclose(fp);
		win_end = buffer_len;
	}
	~Source() {
		delete buf;
	}
};

vector<DictType> initFromSource(Source& source) //@todo template
{
	int string_len = source.win_end - source.win_start;
	vector<DictType> string(string_len);
	for (int i = 0; i < string_len; i++)
		string[i] = source.buf[i + source.win_start];
	return string;
}

struct CompObject {
	Source& source;
	unsigned char* begin;
	DictType len;
	CompObject(Source& source) :source(source) {
		begin = source.buf + source.win_start;
		len = source.win_end - source.win_start;
	}
	bool operator()(DictType a, DictType b) {
		return bcomp(begin + a, begin + b, len - a, len - b) < 0;
	}
};

//A naive, slow suffix - array construction algorithm.
vector<DictType> naivelyMakeSuffixArray(Source& source)
{
    vector<DictType> sa(source.win_end - source.win_start + 1);
	unsigned char* p = source.buf;
	for (int i = 0; i <= source.win_end - source.win_start; i++)
	{
		sa[i] = i;
		p++;
	}
	CompObject co(source);
	sort(sa.begin(), sa.end(), co);
	return sa;
}

struct Sample {
    string filename;
    string desc;
    int numBytes;
    int numLoopsnNaive;
    int numLoopsnAlg;
};

const int numSamples = 4;
const Sample samples[numSamples] = {
        {"alice29.txt", "Standard text from book 'Alice in Wonderland'",100000, 100, 100},
        {"mtpacga","Sample gene, almost random 4/256 values", 100000, 100, 100},
        {"random.txt", "Generated pseudo random text, 64/256 values",100000, 100, 100},
        {"abac", "Long cycle, problem for naive", 30000, 1, 1000}
};

int main()
{
    cout << "current directory=" << current_working_directory() << endl;
	cout << "search res in " << relativeFolder << endl;
    for (int i=0; i<numSamples; i++)
    {
        stopwatch st;
        cout << samples[i].filename << endl;
        cout << samples[i].desc << endl;
        cout << "read " << samples[i].numBytes << " bytes " << endl;

        Source source(samples[i].filename, samples[i].numBytes);

        cout << "naive " << samples[i].numLoopsnNaive << " times:" << endl;
        st.start();
        for (int j=0; j<samples[i].numLoopsnNaive; j++) {
            sa_base = naivelyMakeSuffixArray(source);
        }
        st.stop();
        double naiveTime = st.duration()/samples[i].numLoopsnNaive;
        double perSymbol = naiveTime/ samples[i].numBytes *1e9;
        cout << "naive time " <<  naiveTime << " s per loop" << " = " << perSymbol << " ns per symbol" << endl;

        cout << "algorithm " << samples[i].numLoopsnAlg << " times:" << endl;
        st.start();
        for (int j=0; j<samples[i].numLoopsnAlg; j++) {
            vector<DictType> string = initFromSource(source);
            sa = makeSuffixArrayByInducedSorting(string, 256);
        }
        st.stop();
        double algTime = st.duration()/samples[i].numLoopsnAlg;
        perSymbol = algTime/ samples[i].numBytes *1e9;
        cout << "algorithm time " <<  algTime << " s per loop" << " = " << perSymbol << " ns per symbol" << endl;
        cout << naiveTime/algTime << " x faster " << endl;

        if (sa_base == sa)
            cout << "both methods created the same SA" <<endl;
        else
            cout << "Error, methods created differ SA" <<endl;
        cout << endl;
    }
}
